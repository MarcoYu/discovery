package com.estacionar.main;

import java.nio.file.Path;
import java.nio.file.Paths;

import com.estacionar.impl.NotificadorMail;
import com.estacionar.model.FinderNotificadorImplementation;

public class Main {

	public static void main(String[] args) {
		Path path = Paths.get("");
		String directoryName = path.toAbsolutePath().toString();
		
		FinderNotificadorImplementation finderImplementation = new FinderNotificadorImplementation();
		NotificadorMail notificadorMail = finderImplementation.fisrtImplementation(directoryName+"\\bin\\com\\estacionar\\impl", "com.estacionar.impl.");
				
		notificadorMail.enviarNotificacion();
		
	}

}
