package com.estacionar.impl;

import com.estacionar.interfaz.Notificador;

public class NotificadorMail implements Notificador{
	
	public String mensajeDesde = "Se envio una notificacion por Mail.";

	public NotificadorMail() {
		
	}
	
	@Override
	public void enviarNotificacion() {
		System.out.println(mensajeDesde);
	}

}
