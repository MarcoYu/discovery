package com.estacionar.impl;

import com.estacionar.interfaz.Notificador;

public class NotificadorTelegram implements Notificador{
	
	public String mensajeDesde = "Se envio una notificacion por Telegram.";;

	public NotificadorTelegram() {
		
	}
	
	@Override
	public void enviarNotificacion() {
		System.out.println(mensajeDesde);
	}

}
